/**
 * Bài 3: quy đổi tiền
 * 
 * input:
 * const USD = 23500;
 * var needToExchange:người dùng nhập số tiền USD cần đổi qua VND
 * 
 * toDo:
 * 
 * Thực hiện quy đổi theo công thức 1 USD = 23500 VND;
 * 
 * output:
 * 
 * result: yourMoney = needToExchange*USD;
 * 
 * 
 */
function quyDoiTien() {
    const USD = 23500;
    var needToExchange = prompt("Mời mentor nhập số tiền USD cần đổi");
    var yourMoney = +needToExchange * USD;
    console.log("Tiền VND của bạn là: " + yourMoney);
}

quyDoiTien();






