/**
 * Bài 2: Tính giá trị trung bình của 5 số thực
 * 
 * input:
 * 
 * var mangSoThuc = [];
 * var giaTriTrungBinh = null;
 * var tongGiaTriCuaMang = 0;
 * 
 * toDO:
 *1.cho người dùng nhập vào 5 giá trị 
 *2.Giá trị trung bình = tổng giá trị của mảng / độ dài mảng
 *
 * output:
 * 
 * result: giá trị trung bình 
 */
function tinhGiaTriTrungBinh() {
    var mangSoThuc = [];
    var giaTriTrungBinh;
    var tongGiaTriCuaMang = 0;
    for (var i = 0; i < 5; i++) {
        mangSoThuc[i] = prompt("Mời Mentor nhập vào số thực thứ: " + (i + 1))
        tongGiaTriCuaMang += +mangSoThuc[i];
    }

    giaTriTrungBinh = tongGiaTriCuaMang / mangSoThuc.length;

    console.log("Giá trị trung bình là: " + giaTriTrungBinh);
}
tinhGiaTriTrungBinh();



