/**
 * bài 5: tính tổng 2 kí số
 * input:
 * var number: số nhập vào có 2 chữ số
 * 
 * todo:
 * tách ra hàng chục và hàng đơn vị
 * hàng chục = number/10 và làm tròn;
 * hàng đơn vị = phần dư của number/10
 * 
 * output:
 * var tongKiSo = hangChuc + hangDonVi;
 * 
 */
function tong2KiSo() {
    var number = prompt("Mời mentor nhập SỐ CÓ 2 CHỮ SỐ ạ: ");
    var hangChuc = Math.floor(number / 10);
    var hangDonVi = number % 10;
    var tongKiSo = hangChuc + hangDonVi;
    console.log('tongKiSo: ', tongKiSo);
}
tong2KiSo();


