/**
 * Bài 4: tính diện tích, chu vi hình chữ nhật
 * 
 * input:
 * var chieuDai
 * var chieuRong
 * 
 * todo:
 * áp dụng công thức tính diện tích và chu vi hình chữ nhật
 * diện tích = dài * rộng
 * chu vi = (dài + rộng)*2
 * 
 * output:
 * var dienTich = chieuDai * chieuRong;
 * var chuVi = (chieuDai + chieuRong) * 2;
 */
function tinhHinhChuNhat() {
    var chieuDai = prompt("Mời mentor nhập chiều dài");
    var chieuRong = prompt("Mời mentor nhập chiều rộng");

    var dienTich = chieuDai * chieuRong;

    var chuVi = (+chieuDai + +chieuRong) * 2;


    console.log('Diện tích hình chữ nhật là: ', dienTich);
    console.log('Chu vi hình chữ nhật là: ', chuVi);
}
tinhHinhChuNhat();



